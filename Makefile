compose_deploy:
	docker-compose -f docker-compose.yaml up -d

compose_clean:
	docker-compose -f docker-compose.yaml down
